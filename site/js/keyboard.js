var capsLock = document.getElementById("caps-lock");
var capsLed = document.querySelector(".caps-led");
var letters = document.querySelectorAll(".letter");
var lock = false;
var shiftOF = false;

var modeCheck = document.querySelector(".switch>input");
var textZone = document.getElementById("text-zone");

//actions au chargement de la page
window.onload = function () {
	modeCheck.checked = false; //checkbox décoché
};

/****gestion des touches****/
var kbdKeys = document.querySelectorAll(".key");
kbdKeys.forEach( function(element, index) {
	element.addEventListener("click", keyClick, false); //ajout de l'écouteur d'evenement
});

function keyClick (arg) {
	var kbdKey = arg.currentTarget;	
	if (kbdKey.childNodes.length>1) {
		var children = kbdKey.childNodes;
		textZone.value += children[0].innerText;
	}else {
		textZone.value += kbdKey.innerText;
	}	
}
//touche espace
var spaceKey = document.getElementById("spaceKey");
spaceKey.onclick = function () {
	textZone.value += " ";
};
//touche entrée
var enterKey = document.getElementById("enterKey");
enterKey.onclick = function () {
	textZone.value += "\n";
};
//touche effacer
var clearKey = document.getElementById("clearKey");
clearKey.onclick = function () {
	textZone.value = textZone.value.substring(0, textZone.value.length-1);
};
//touches shift
var shiftKeys = document.querySelectorAll(".shiftKey");
shiftKeys.forEach( function(element, index) {
	element.addEventListener("click", shiftClick, false); //ajout de l'écouteur d'evenement
});

function shiftClick (arg) {
	shiftKeys.forEach( function(element, index) {
		element.classList.toggle("scaleKey");
	});
	if (lock) {
		if (shiftOF) {
			capsLockOn();
			shiftOF = false;
		} else {
			capsLockOff();
			shiftOF = true;
		}
	} else {		
		if (shiftOF) {
			capsLockOff();
			shiftOF = false;
		} else {
			capsLockOn();
			shiftOF = true;
		}
	}
}
/****fin gestion des touches****/

//switch entre light et night mode
modeCheck.onclick = function () {
	var elements = document.querySelectorAll(".light")
	elements.forEach( function(element, index) {
		element.classList.toggle("night");
	});
};

//gestion du clic sur la touche caps Lock
capsLock.onclick = function () {
	if (shiftOF) {
		if (lock) {
			capsLockOn();
			capsLed.style.backgroundColor = "#FFF";
			lock = false;
		} else {
			capsLockOff();
			capsLed.style.backgroundColor = "#17a2b8";
			lock = true;
		}
	} else {
		if (lock) {
			capsLockOff();
			capsLed.style.backgroundColor = "#FFF";
			lock = false;
		} else {
			capsLockOn();
			capsLed.style.backgroundColor = "#17a2b8";
			lock = true;
		}
	}
};
//fonction activation caps lock
function capsLockOn () {
	letters.forEach( function(element, index) {
		element.innerText = element.innerText.toUpperCase();
	});	
}
//fonction désactivation caps lock
function capsLockOff () {
	letters.forEach( function(element, index) {
		element.innerText = element.innerText.toLowerCase();
	});	
}

//console.log(capsLock);
//capsLockOn();
//console.log(letters);